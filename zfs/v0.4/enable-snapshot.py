#!/usr/bin/env python3
# coding: utf-8

# $Id: enable-snapshot.py 936 2019-01-28 11:10:16Z gruiick $
# SPDX-License-Identifier: BSD-2-Clause

"""
    * create a config file for zfs volume provided by '-n name', ask
      if it is correct before writing,
    * create crontab file for zfs volume provided by '-n name', ask
      if it is correct before writing,
    * ask and restart cron.

TODO:

FIXME:

"""
__version__ = "0.3"
__author__ = "See AUTHORS"
__copyright__ = "Copyright 2018, PSMN, ENS de Lyon"
__credits__ = "See CREDITS"
__license__ = "BSD-2"
__maintainer__ = "Loïs Taulelle"
__email__ = "None"
__status__ = "Production"

import os
import sys
import yaml
import argparse
import distutils.util
import execo
import logging

from pprint import pprint
from datetime import datetime


# global configuration file
configfile = '/root/conf/zfs-defaults.yml'


def main():
    """
        verify admin rights
        load default config file
        set log level & log file
        using args and config dict, prepare yml config dict
        ask user, then save config dict (as yaml file)
        prepare crontab file
        ask user, then save it
        ask user, restart cron daemon
    """
    # exit if not euid0
    verifRoot()

    # read global config from a yml file
    gconfig = loadYamlFile(configfile)
    
    gconfig['date'] = datetime.now().strftime('%Y/%m/%d-%H:%M:%S')

    args = get_options()
    # prepare configuration
    yconfig = {'general': {},
               'action': 'snapshot',
               'snapshot': {},
               }
    if args.debug:
        yconfig['general']['debug'] = 'yes'
        execo.log.logger.setLevel('DEBUG')
        execo.log.logger.debug(args)
        execo.log.logger.debug(gconfig)
    else:
        yconfig['general']['debug'] = 'no'
        execo.log.logger.setLevel('INFO')

    if gconfig['version'] != __version__:
        execo.log.logger.critical('Configuration version mismatch. Should be version ' +
                                  __version__)
        sys.exit(1)
    else:
        yconfig['general']['version'] = __version__

    if args.log:
        yconfig['general']['log'] = 'yes'
        logfile = gconfig['path']['log'] + 'zfs-snapshot.log'
        yconfig['general']['logfile'] = logfile
        file_handler = logging.FileHandler(logfile, 'a')
        # apply 'execo' formatter to file_handler
        file_handler.setFormatter(execo.logger.handlers[0].formatter)
        # create logpath before addHandler
        verifyOrCreateDir(logfile)
        execo.log.logger.addHandler(file_handler)
    else:
        yconfig['general']['log'] = 'no'
        # even if no log, specify log file, one may change his mind
        logfile = gconfig['path']['log'] + 'zfs-snapshot.log'
        yconfig['general']['logfile'] = logfile

    if args.name:
        # TODO: test existence of zfs volume?
        yconfig['snapshot']['name'] = str(args.name[0])
    else:
        # this should be impossible.
        execo.log.logger.critical("please provide zfs volume's name")
        sys.exit(1)

    # FIXME: there was a distinction between fs and volume in the
    # original tool, don't know why...
    #if args.f:
        #yconfig['type'] = 'filesystem'
    #elif args.v:
        #yconfig['type'] = 'volume'

    if args.retention:
        yconfig['retention'] = str(args.retention[0])
    else:
        yconfig['retention'] = '7'

    # check and save configuration file
    nomfichier = 'snapshot-' + str(args.name[0]).replace('/', '-')
    pprint(yconfig)
    verifconf = queryYesNo('Save this configuration:')
    if verifconf == 1:
        tosave = gconfig['path']['conf'] + nomfichier + '.yml'
        verifyOrCreateDir(tosave)
        saveYamlFile(tosave, yconfig)
        execo.log.logger.info(str(tosave) + ' saved \n')
    else:
        execo.log.logger.info('nothing to do.')
        sys.exit(0)

    # prepare crontab, all in str()
    minute = str(args.minute[0])
    hour = str(args.hour[0])
    if args.dayofmonth:
        dom = str(args.dayofmonth[0])
    else:
        dom = '*'
    if args.month:
        month = str(args.month[0])
    else:
        month = '*'
    if args.dayofweek:
        dow = str(args.dayofweek[0])
    else:
        dow = '*'

    # save crontab
    pybin = gconfig['path']['snapshot']
    if not args.mail:
        cronlist = (minute, hour, dom, month, dow, gconfig['user'],
                    pybin, '-c', tosave, '>/dev/null')
        mailto = 'MAILTO=root'
    else:
        cronlist = (minute, hour, dom, month, dow, gconfig['user'],
                    pybin, '-c', tosave)
        mailto = 'MAILTO=' + str(args.mail)

    crontab = (' '.join(cronlist))
    print(mailto)
    print(crontab)
    verifcron = queryYesNo('Save this crontab:')
    if verifcron == 1:
        cronfichier = gconfig['path']['cron'] + nomfichier
        defaultpath = 'PATH=/sbin:/bin:/usr/sbin:/usr/bin'
        fichier = ['# ' + gconfig['date'], mailto, defaultpath, crontab, '']
        verifyOrCreateDir(cronfichier)
        saveCronTab(cronfichier, fichier)
        execo.log.logger.info(cronfichier + ' saved \n')

        # restart cron
        recron = queryYesNo('Restart cron daemon')
        if recron == 1:
            cmd = 'systemctl restart cron.service'
            executeProcess(cmd)


def loadYamlFile(yamlfile):
    """
        load data from a yaml file, using safe_load, return a dict{}.
        yamlfile is mandatory. Throw yaml errors, if any.
    """
    try:
        with open(yamlfile, 'r') as fichier:
            contenu = yaml.safe_load(fichier)
            return(contenu)
    except IOError:
        print('Unable to read file: ', fichier.name)
        sys.exit(1)
    except yaml.YAMLError as erreur:
        if hasattr(erreur, 'problem_mark'):
            mark = erreur.problem_mark
            print('YAML error position: (%s:%s) in ' % (mark.line + 1,
                  mark.column + 1), fichier.name)
        sys.exit(1)


def saveYamlFile(yamlfile, data):
    """
        Save data into yamlfile, using safe_dump
        yamlfile is mandatory
        data must be a dict{}
    """
    try:
        with open(yamlfile, 'w') as fichier:
            fichier.write('%YAML 1.1\n---\n')
            yaml.safe_dump(data, stream=fichier, default_flow_style=False)
    except EnvironmentError as e:
        execo.log.logger.critical('Environment Error: ' + e.strerror +
                                  ' ' + e.errno + ' ' + e.filename)
        sys.exit(1)
    except yaml.YAMLError as erreur:
        execo.log.logger.critical('YAML error: ' + erreur + ', ' + fichier.name)
        sys.exit(1)


def queryYesNo(question):
    """
        Ask a yes/no question via input() and return 1 if y(es), 0 elsewhere.
        no default
    """
    print('\n' + question + ' [y/n]?')
    while True:
        try:
            return distutils.util.strtobool(input().lower())
        except ValueError:
            print('Please reply "y" or "n".')


def verifyOrCreateDir(filename):
    """
        Verify if path exist (before a file will be created)
        create path if doesn't exist
    """
    execo.log.logger.debug(filename)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
        except OSError as e:
            execo.log.logger.critical('Cannot create directory: ' + e.strerror)
            sys.exit(1)


def verifRoot():
    """
        Verify if euid 0, if not, exit(1)
    """
    if int(os.geteuid()) != 0:
        execo.log.logger.critical('You are not root. Use sudo or biroute.')
        sys.exit(1)


def saveCronTab(nomfichier, contenufichier):
    """
        Write crontab file
    """
    try:
        with open(nomfichier, 'w') as fic:
            fic.write('\n'.join(contenufichier))
            fic.write('\n')
    except EnvironmentError as e:
        execo.log.logger.critical('Environment Error: ' + e.strerror +
                                  ' ' + e.errno + ' ' + e.filename)
        sys.exit(1)


def executeProcess(commande):
    """
        execute commande, return nothing
    """
    with execo.process.Process(commande).run() as process:
        if process.exit_code != 0:
            execo.log.logger.critical('process:\n' + str(process))
            execo.log.logger.warning('process stdout:\n' + process.stdout)
            execo.log.logger.warning('process stderr:\n' + process.stderr)


def get_options():
    """
        read parser and return args (as args namespace)
    """
    parser = argparse.ArgumentParser(description='Configuration and crontab for automated snapshot')
    group1 = parser.add_argument_group('conf', description='configuration file options')
    group1.add_argument('-d', '--debug', action='store_true', help='toggle debug (default: no)')
    group1.add_argument('-l', '--log', action='store_true', help='toggle logs (default: no)')
    group1.add_argument('-n', '--name', nargs=1, type=str, help='zfs volume name',
                        required=True)
    group1.add_argument('-r', '--retention', nargs=1, type=int, help='retention, in day(s) (default: 7)')
    group1.add_argument('--mail', action='store', nargs='?', type=str, const='root',
                        help='let cron send an email each snapshot (default: no, [default: root])')
    #group2 = parser.add_mutually_exclusive_group()
    #group2.add_argument('-f', action='store_true', help='zfs type: Filesystem (default)',
                        #default=True)
    #group2.add_argument('-v', action='store_true', help='zfs type: Volume')
    group3 = parser.add_argument_group('cron', description='crontab options:')
    group3.add_argument('-H', '--hour', nargs=1, type=int, help='cron hour parameter',
                        required=True)  # int, for sure ?
    group3.add_argument('-m', '--minute', nargs=1, type=int, help='cron minute parameter',
                        required=True)
    group3.add_argument('-DM', '--dayofmonth', nargs=1, type=int,
                        help='cron day of month parameter')
    group3.add_argument('-M', '--month', nargs=1, type=int,
                        help='cron month parameter')
    group3.add_argument('-DW', '--dayofweek', nargs=1, type=int,
                        help='cron day of week parameter')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
