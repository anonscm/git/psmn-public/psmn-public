#!/usr/bin/env python3
# coding: utf-8

# $Id: zfs-replica.py 950 2019-02-07 16:27:59Z gruiick $
# SPDX-License-Identifier: BSD-2-Clause

"""
    * read config file (given by -c argument),
    * replicate snapshot(s), full or incremental
    * list replicat(s), if replicat is oldest than retention, erase it.

"""
__version__ = "0.4"
__author__ = "See AUTHORS"
__copyright__ = "Copyright 2018, PSMN, ENS de Lyon"
__credits__ = "See CREDITS"
__license__ = "BSD-2"
__maintainer__ = "Loïs Taulelle"
__email__ = "None"
__status__ = "Production"

import sys
import os
import argparse
import distutils.spawn
import distutils.util
import datetime
import logging
from pprint import pprint
import yaml

import execo


# global configuration file
# configfile = "/root/conf/zfs-defaults.yml"
# no need of default config file for now
gconfig = {'zfs': '', }


def main():
    """
        verify admin rights
        load config file
        set log level & log file
        check if zfs binary exist
        full replication or
        incremental replication
        delete replica(s), if needed
    """

    verif_root()
    args = get_options()

    # load configuration
    fichierconf = str(args.conf[0])
    config = load_config(fichierconf)

    if args.debug:
        pprint(config)

    # there MUST be a config['action'] verification
    if config['action'] != 'replica':
        execo.log.logger.critical('Configuration error: should be replica.')
        sys.exit(1)

    if config['general']['debug'] == 'yes' or args.debug:
        execo.log.logger.setLevel('DEBUG')
        execo.log.logger.debug(gconfig)
        execo.log.logger.debug(config)
    else:
        execo.log.logger.setLevel('INFO')

    if config['general']['log'] == 'yes':
        logfile = config['general']['logfile']
        file_handler = logging.FileHandler(logfile, 'a')
        # apply 'execo' formatter to file_handler
        file_handler.setFormatter(execo.logger.handlers[0].formatter)
        # create logpath before addHandler
        verify_or_create_dir(logfile)
        execo.log.logger.addHandler(file_handler)

    if config['general']['version'] != __version__:
        execo.log.logger.critical('Configuration version mismatch. Should be version ' +
                                  __version__)
        sys.exit(1)

    gconfig['zfs'] = str(verif_exec('zfs'))

    rtype = config['replica']['type']  # simple/complete
    rarchive = config['replica']['archive']  # oldest/latest

    srcname = config['replica']['sourcename']
    tgtpool = config['replica']['targetpool']

    if tgtpool is None:
        tgtname = config['replica']['sourcename']
    else:
        if rtype == 'complete':
            # include complete source pool in target dataset
            tgtname = tgtpool + '/' + srcname
        elif rtype == 'simple':
            # add last part of source dataset to target pool
            source_lastpart = srcname.rpartition('/')[2]
            tgtname = tgtpool + '/' + source_lastpart

    srv = config['replica']['server']

    if srv == 'localhost' and tgtpool is None:
        execo.log.logger.critical('Configuration error: cannot replicate on same pool')
        sys.exit(1)

    replication = get_dist_snap(srv, tgtname, first=True)

    if replication == 'full':
        # first, create target dataset on target host
        target_cmd = gconfig['zfs'] + ' create -p ' + tgtname
        execo.log.logger.debug(srv + ': ' + target_cmd)
        execute_zfs(target_cmd, host=srv)

        full_replication(srv, srcname, tgtname, archtype=rarchive)

    else:
        # replication contain list of already replicated snapshots on
        # target, but we don't care, as incremental_replication() can
        # also be called from full_replication().
        incremental_replication(srv, srcname, tgtname)

    # erase replica(s) older than retention
    borne = datetime.timedelta(days=int(config['retention']))
    destroy_replica(srv, tgtname, borne)


def destroy_replica(serveur, tgtzname, borne):
    """ filter and destroy replica(s) older than retention """

    # Refresh the target snapshot list
    snapdates = get_dist_snap(serveur, tgtzname)

    tgtdates = filter_date_snap(snapdates)

    lastdate = max(tgtdates)

    for timestp in tgtdates:
        if lastdate - borne <= timestp:
            execo.log.logger.debug('keep: ' + str(timestp))
        else:
            to_erase = tgtzname + '@snapshot-' + str(timestp.strftime('%Y%m%d%H%M%S'))
            execo.log.logger.debug('erase: ' + to_erase)

            cmd = gconfig['zfs'] + ' destroy ' + to_erase
            execo.log.logger.info('erasing: ' + to_erase + ' on host ' + serveur)
            execute_zfs(cmd, host=serveur)


def incremental_replication(serveur, localzname, tgtzname):
    """
        do incremental replication using latest tgt and latest src
        find latest snap on target
            use same date on source
        find latest snap on source
        if latest target is less than latest src : replicate snapshot(s)
    """
    # We need to refresh the target snapshot list, as we can be called
    # by full_replication, on oldest mode, a long time after full has
    # started (hours, days).
    # Make sure retention is enought to perform a first replication, in time,
    # with oldest source snapshot before it get destroy on local dataset.

    snaplist = get_dist_snap(serveur, tgtzname)
    tgtdates = filter_date_snap(snaplist)
    latest_tgt_date = max(tgtdates)  # datetime
    latest_tmp_snap = localzname + '@snapshot-' + str(latest_tgt_date.strftime('%Y%m%d%H%M%S'))

    latestsnap = []
    latestsnap.append(get_snap(localzname, status='latest'))

    srcdate = filter_date_snap(latestsnap)
    latest_src_date = max(srcdate)  # datetime

    execo.log.logger.debug("latest tgt :" + str(latest_tmp_snap))
    execo.log.logger.debug("latest src :" + str(latestsnap[0]))

    # send diff 'latest on target' vs 'latest on source'
    if latest_tgt_date < latest_src_date:
        lcl_cmd = gconfig['zfs'] + ' send -I ' + latest_tmp_snap + " " + latestsnap[0]
        tgt_cmd = gconfig['zfs'] + ' receive ' + tgtzname

        if serveur == 'localhost':
            complete_cmd = lcl_cmd + ' | ' + tgt_cmd
        else:
            complete_cmd = lcl_cmd + ' | ssh -q -o BatchMode=yes root@' + serveur + ' ' + tgt_cmd

        execo.log.logger.debug(complete_cmd)
        execute_zfs(complete_cmd, shell=True)

        execo.log.logger.info('copying (up to): ' + latestsnap[0] + ' on host ' + serveur)
    else:
        execo.log.logger.info('nothing to do.')


def full_replication(serveur, localzname, tgtzname, archtype=None):
    """
        if archtype is oldest
            send oldest snapshot
            then switch to incremental
        else (None or latest)
            send latest snapshot
    """
    if archtype == 'oldest':
        localsnap = get_snap(localzname, status='oldest')
    else:  # archtype is latest or None
        localsnap = get_snap(localzname, status='latest')

    local_cmd = gconfig['zfs'] + ' send ' + localsnap
    execo.log.logger.debug(local_cmd)

    tgt_cmd = gconfig['zfs'] + ' receive -F ' + tgtzname
    execo.log.logger.debug(tgt_cmd)

    # simpliest: local_cmd | ssh -o BatchMode=yes root@serveur tgt_cmd
    if serveur == 'localhost':
        complete_cmd = local_cmd + ' | ' + tgt_cmd
    else:
        complete_cmd = local_cmd + ' | ssh -q -o BatchMode=yes root@' + serveur + ' ' + tgt_cmd

    execo.log.logger.debug(complete_cmd)
    execo.log.logger.info('copying: ' + localsnap + ' on host ' + serveur)
    execute_zfs(complete_cmd, shell=True)

    # should be this way with mbuffer
    # with execo.process.SshProcess(tgt_cmd, serveur).start() as receiver:
    #    execo.sleep(2)
    #    sender = execo.process.Process(local_cmd).run()
    #    receiver.wait()
    # print(receiver.stdout)

    if archtype == 'oldest':
        # goto incremental
        execo.log.logger.debug('doing incremental')
        incremental_replication(serveur, localzname, tgtzname)


def get_snap(zvolname, status=None):
    """
        if status is oldest, return oldest snapshot
        if status is latest, return latest snapshot
        else return snapshots list from a zfs volume
    """

    cmd = gconfig['zfs'] + ' list -H -o name -t snapshot -r ' + zvolname
    str_snap = execute_zfs(cmd)
    lst_snap = str_snap.splitlines()

    snapdates = filter_date_snap(lst_snap)

    if status == 'oldest':
        snapdate = min(snapdates)
        snap = zvolname + '@snapshot-' + str(snapdate.strftime('%Y%m%d%H%M%S'))
        return snap
    elif status == 'latest':
        snapdate = max(snapdates)
        snap = zvolname + '@snapshot-' + str(snapdate.strftime('%Y%m%d%H%M%S'))
        return snap
    else:
        # status is anything else, including None
        return lst_snap


def filter_date_snap(liste_snap):
    """ filter list of snapshots, return list of dates """

    lstsnapdates = []

    for element in liste_snap:
        # keep the second part of snapshot name, as a date
        # ignore snapshots which aren't in the format "dataset@snapshot-%Y%m%d%H%M%S"
        try:
            # avoid dataset part, as it can contain '-'
            temppartition = element.partition('@')[2]
            execo.log.logger.debug('volume: ' + element.partition('@')[0])
            # we're only insterested in the date part
            tempdate = datetime.datetime.strptime(temppartition.partition('-')[2], '%Y%m%d%H%M%S')
            execo.log.logger.debug('date: ' + str(tempdate))
            lstsnapdates.append(tempdate)
        except ValueError:
            execo.log.logger.debug('ignored: ' + str(element))
            pass

    return lstsnapdates


def get_dist_snap(serveur, zvolname, first=False):
    """
        list snapshots on target host, if None, return 'full'
    """
    cmd = gconfig['zfs'] + ' list -H -o name -t snapshot -r ' + zvolname

    if first:
        # target dataset may not exist
        str_snap = execute_zfs(cmd, host=serveur, nolog=True)
    else:
        str_snap = execute_zfs(cmd, host=serveur)

    #execo.log.logger.debug(type(str_snap))
    execo.log.logger.debug(str_snap)

    if str_snap is None:
        # full, for new replication
        execo.log.logger.info('No replica(s), doing full')

        return 'full'
    else:
        # return snapshots list for incremental replication
        lst_snap = str_snap.splitlines()
        execo.log.logger.debug(lst_snap)

        return lst_snap


def verif_root():
    """
        Verify if euid 0, if not, exit(1)
    """
    if int(os.geteuid()) != 0:
        execo.log.logger.critical('You are not root. Use sudo or biroute.')
        sys.exit(1)


def verify_or_create_dir(filename):
    """
        Verify if path exist (before a file will be created)
        create path if it doesn't exist
    """
    execo.log.logger.debug(filename)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
        except OSError as err:
            execo.log.logger.critical('Cannot create directory: ' + err.strerror)
            sys.exit(1)


def verif_exec(binaire):
    """
        'which'-like function
        return the complete path of 'binaire', if found in $PATH
    """
    try:
        cmd_exist = distutils.spawn.find_executable(binaire)
        if cmd_exist is None:
            execo.log.logger.warning('Command not found: ' + binaire)
        else:
            execo.log.logger.debug('Command found: ' + cmd_exist)
            return cmd_exist
    except OSError as error:
        execo.log.logger.critical('Execution failed: ' + error)


def execute_zfs(zfscmd, host=None, shell=False, nolog=False):
    """
        execute zfs command, return stdout as a string or None
        if host, use SshProcess (remote)
        if shell, use shell=True (accept '|' in zfscmd)
        if nolog, use nolog_exit_code=True (don't go CRITICAL rightaway)
            process zfs expected error 'dataset does not exist'

    """

    if host not in [None, 'localhost']:
        if nolog:
            process = execo.process.SshProcess(zfscmd, host, nolog_exit_code=True).run()
        else:
            process = execo.process.SshProcess(zfscmd, host).run()

    elif shell:
        process = execo.process.Process(zfscmd, shell=True).run()

    elif nolog:
        process = execo.process.Process(zfscmd, nolog_exit_code=True).run()

    else:
        process = execo.process.Process(zfscmd).run()

    # only if nolog_exit_code=True, else backfire already occured
    if process.exit_code != 0:
        # NOTE: every zfs error code is 256.
        # best to search "dataset does not exist" in stdout
        if process.stdout.count('dataset does not exist') == 1:  # or > 0 ?
            execo.log.logger.debug('This is expected: dataset does not exist')
            return None
        else:  # something went wrong, explode nicely
            execo.log.logger.critical('process:\n' + str(process))
            execo.log.logger.warning('process stdout:\n' + process.stdout)
            execo.log.logger.warning('process stderr:\n' + process.stderr)
            sys.exit(1)

    else:  # everything is OK, return stdout
        execo.log.logger.debug(type(process.stdout))
        return process.stdout


def load_config(yamlfile):
    """ Load configuration from yamlfile, return a dict

        yamlfile is mandatory, using safe_load
        Throw yaml errors, with positions, if any, and quit.
        return a dict
    """
    try:
        with open(yamlfile, 'r') as fichier:
            contenu = yaml.safe_load(fichier)
            return contenu
    except IOError:
        execo.log.logger.critical('Unable to read/load config file: ' +
                                  fichier.name)
        sys.exit(1)
    except yaml.MarkedYAMLError as erreur:
        if hasattr(erreur, 'problem_mark'):
            mark = erreur.problem_mark
            msg_erreur = "YAML error position: ({}:{}) in ".format(mark.line + 1,
                                                                   mark.column)
            execo.log.logger.critical(msg_erreur + str(fichier.name))
        sys.exit(1)


def get_options():
    """
        read parser and return args (as args namespace)
    """
    parser = argparse.ArgumentParser(description='Replicate and delete automated snapshots')
    parser.add_argument('-c', '--conf', nargs=1, type=str, help='Mandatory config file', required=True)
    parser.add_argument('-d', '--debug', action='store_true', help='toggle debug (default: no)')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
