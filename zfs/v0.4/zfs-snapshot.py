#!/usr/bin/env python3
# coding: utf-8

# $Id: zfs-snapshot.py 939 2019-01-29 09:59:34Z gruiick $
# SPDX-License-Identifier: BSD-2-Clause

"""
    * read config file (given by -c argument),
    * create snapshot,
    * list snapshots, if snapshot is oldest than retention, erase it.

"""
__version__ = "0.3"
__author__ = "See AUTHORS"
__copyright__ = "Copyright 2018, PSMN, ENS de Lyon"
__credits__ = "See CREDITS"
__license__ = "BSD-2"
__maintainer__ = "Loïs Taulelle"
__email__ = "None"
__status__ = "Production"

import sys
import os
import argparse
import distutils.spawn
import distutils.util
import datetime
import yaml
import execo
import logging


# global configuration file
# configfile = "/root/conf/zfs-defaults.yml"
# no need of default config file for now
gconfig = {'zfs': '', }


def main():
    """
        verify admin rights
        load config file
        set log level & log file
        check if zfs binary exist
        create snapshot
        delete snapshot(s), if needed
    """

    verifRoot()
    args = get_options()

    # load snapshot configuration
    fichierconf = str(args.conf[0])
    config = loadConfig(fichierconf)

    if config['general']['debug'] == 'yes' or args.debug:
        execo.log.logger.setLevel('DEBUG')
        execo.log.logger.debug(gconfig)
        execo.log.logger.debug(config)
    else:
        execo.log.logger.setLevel('INFO')

    if config['general']['log'] == 'yes':
        logfile = config['general']['logfile']
        file_handler = logging.FileHandler(logfile, 'a')
        # apply 'execo' formatter to file_handler
        file_handler.setFormatter(execo.logger.handlers[0].formatter)
        # create logpath before addHandler
        verifyOrCreateDir(logfile)
        execo.log.logger.addHandler(file_handler)

    # Check version
    if config['general']['version'] != __version__:
        execo.log.logger.critical('Configuration version mismatch. Should be version ' +
                                  __version__)
        sys.exit(1)

    # TODO: there should be a config['action'] verification
    if config['action'] != 'snapshot':
        execo.log.logger.critical('Configuration error: should be snapshot.')
        sys.exit(1)

    gconfig['zfs'] = str(verifExec('zfs'))

    volname = config['snapshot']['name']
    retention = config['retention']

    createSnapshot(volname)

    deleteSnapshot(volname, retention)


def verifRoot():
    """
        Verify if euid 0, if not, exit(1)
    """
    if int(os.geteuid()) != 0:
        execo.log.logger.critical('You are not root. Use sudo or biroute.')
        sys.exit(1)


def verifyOrCreateDir(filename):
    """
        Verify if path exist (before a file will be created)
        create path if it doesn't exist
    """
    execo.log.logger.debug(filename)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
        except OSError as e:
            execo.log.logger.critical('Cannot create directory: ' + e.strerror)
            sys.exit(1)


def verifExec(binaire):
    """
        'which'-like function
        return the complete path of 'binaire', if found in $PATH
    """
    try:
        cmd_exist = distutils.spawn.find_executable(binaire)
        if cmd_exist is None:
            execo.log.logger.warning('Command not found: ' + binaire)
        else:
            execo.log.logger.debug('Command found: ' + cmd_exist)
            return cmd_exist
    except OSError as error:
        execo.log.logger.critical('Execution failed: ' + error)


def executeProcess(commande):
    """
        execute commande, return stdout
    """
    with execo.process.Process(commande).run() as process:
        if process.exit_code != 0:
            execo.log.logger.critical('process:\n' + str(process))
            execo.log.logger.warning('process stdout:\n' + process.stdout)
            execo.log.logger.warning('process stderr:\n' + process.stderr)
        else:
            execo.log.logger.debug(type(process.stdout))
            return process.stdout


def loadConfig(yamlfile):
    """
        Load data from yamlfile, using safe_load
        yamlfile is mandatory
        return a dict{}
    """
    try:
        with open(yamlfile, 'r') as fichier:
            contenu = yaml.safe_load(fichier)
            return(contenu)
    except IOError:
        execo.log.logger.critical('Unable to read/load config file: ' +
                                  fichier.name)
        sys.exit(1)
    except yaml.YAMLError as erreur:
        if hasattr(erreur, 'problem_mark'):
            mark = erreur.problem_mark
            execo.log.logger.critical('YAML error position: (%s:%s) in ' +
                                      fichier.name % (mark.line + 1, mark.column + 1))
        sys.exit(1)


def createSnapshot(zvolname):
    """
        create a snapshot of zvolname
        format is <zvolname>@snapshot-date
    """
    zdate = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    snapname = zvolname + "@snapshot-" + zdate
    cmd = gconfig['zfs'] + " snapshot " + snapname
    executeProcess(cmd)
    execo.log.logger.info("snapshot: " + snapname)


def deleteSnapshot(zvolname, retention):
    """
        list snapshot(s) of zvolname
        delete older(s) than retention
    """
    snapdates = []
    borne = datetime.timedelta(days=int(retention))
    cmd = gconfig['zfs'] + ' list -H -o name -t snapshot -r ' + zvolname
    str_snap = executeProcess(cmd)
    lst_snap = str_snap.splitlines()

    execo.log.logger.debug(type(str_snap))
    execo.log.logger.debug(type(lst_snap))
    execo.log.logger.debug(lst_snap)

    for element in lst_snap:
        # keep the second part of snapshot name, as a date
        # ignore snapshots which aren't in the format "<zvolname>@snapshot-%Y%m%d%H%M%S"
        try:
            # avoid zvolname part, as it can contain '-'
            temppartition = element.partition('@')[2]
            execo.log.logger.debug('volume: ' + element.partition('@')[0])
            # we're only insterested in the date part
            tempdate = datetime.datetime.strptime(temppartition.partition('-')[2], '%Y%m%d%H%M%S')
            execo.log.logger.debug('date: ' + str(tempdate))
            snapdates.append(tempdate)
        except ValueError:
            execo.log.logger.debug('ignored: ' + str(element))
            pass

    lastdate = max(snapdates)
    execo.log.logger.debug('lastdate: ' + str(lastdate))
    for timestp in snapdates:
        if lastdate - borne <= timestp:
            execo.log.logger.debug('keep: ' + str(timestp))
        else:
            to_erase = zvolname + '@snapshot-' + str(timestp.strftime('%Y%m%d%H%M%S'))
            execo.log.logger.debug('erase: ' + str(timestp))
            execo.log.logger.debug(to_erase)
            cmd = gconfig['zfs'] + ' destroy ' + to_erase
            executeProcess(cmd)
            execo.log.logger.info('erased: ' + to_erase)

    lastsnap = zvolname + '@snapshot-' + str(lastdate.strftime('%Y%m%d%H%M%S'))
    execo.log.logger.debug('calculated lastsnap: ' + str(lastsnap))


def get_options():
    """
        read parser and return args (as args namespace)
    """
    parser = argparse.ArgumentParser(description='Create and delete automated snapshots')
    parser.add_argument('-c', '--conf', nargs=1, type=str, help='Mandatory config file', required=True)
    parser.add_argument('-d', '--debug', action='store_true', help='toggle debug (default: no)')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
