#!/usr/bin/env python3
# coding: utf-8

# $Id: disable-snapshot.py 936 2019-01-28 11:10:16Z gruiick $
# SPDX-License-Identifier: BSD-2-Clause

"""
    * erase crontab file for zfs volume provided by '-n name', ask
      before doing it,
    * ask and restart cron,
    * erase config file for zfs volume provided by '-n name', ask before
      doing it.

TODO:

FIXME:

"""
__version__ = "0.3"
__author__ = "See AUTHORS"
__copyright__ = "Copyright 2018, PSMN, ENS de Lyon"
__credits__ = "See CREDITS"
__license__ = "BSD-2"
__maintainer__ = "Loïs Taulelle"
__email__ = "None"
__status__ = "Production"

import os
import sys
import argparse
import distutils.util
import execo
import logging
import yaml


# global configuration file
configfile = "/root/conf/zfs-defaults.yml"


def main():
    """
        verify admin rights
        load default config file
        set log level & log file
        using args and config dict, compute file's names and paths
        ask user, then erase crontab file
        ask user, then erase yaml config file
        ask user, restart cron daemon
    """
    verifRoot()
    args = get_options()

    # read global config from a yml file
    gconfig = loadYamlFile(configfile)

    if args.debug:
        execo.log.logger.setLevel('DEBUG')
    else:
        execo.log.logger.setLevel('INFO')

    if gconfig['version'] != __version__:
        execo.log.logger.critical('Configuration version mismatch. Should be version ' +
                                  __version__)
        sys.exit(1)

    if args.log:
        logfile = gconfig['path']['log'] + 'zfs-snapshot.log'
        file_handler = logging.FileHandler(logfile, 'a')
        # apply 'execo' formatter to file_handler
        file_handler.setFormatter(execo.logger.handlers[0].formatter)
        # create logpath before addHandler
        verifyOrCreateDir(logfile)
        execo.log.logger.addHandler(file_handler)

    nomfichier = 'snapshot-' + str(args.name[0]).replace('/', '-')
    cronfichier = gconfig['path']['cron'] + nomfichier
    if os.path.isfile(cronfichier):
        supprime = queryYesNo('Erase ' + str(cronfichier))
        if supprime == 1:
            eraseFile(cronfichier)

            conffichier = gconfig['path']['conf'] + nomfichier + '.yml'
            if os.path.isfile(conffichier):
                supprime2 = queryYesNo('Erase ' + str(conffichier))
                if supprime2 == 1:
                    eraseFile(conffichier)
            else:
                execo.log.logger.warning('file does not exist: ' + conffichier)

            redemarre = queryYesNo('Restart cron')
            if redemarre == 1:
                cmd = 'systemctl restart cron.service'
                executeProcess(cmd)
    else:
        execo.log.logger.warning('file does not exist: ' + cronfichier)


def loadYamlFile(yamlfile):
    """
        load data from a yaml file, using safe_load, return a dict{}.
        yamlfile is mandatory. Throw yaml errors, if any.
    """
    try:
        with open(yamlfile, 'r') as fichier:
            contenu = yaml.safe_load(fichier)
            return(contenu)
    except IOError:
        print('Unable to read file: ', fichier.name)
        sys.exit(1)
    except yaml.YAMLError as erreur:
        if hasattr(erreur, 'problem_mark'):
            mark = erreur.problem_mark
            print('YAML error position: (%s:%s) in ' % (mark.line + 1,
                  mark.column + 1), fichier.name)
        sys.exit(1)


def queryYesNo(question):
    """
        Ask a yes/no question via input() and return 1 if yes, 0 elsewhere.
    """
    print('\n' + question + ' [y/n]?')
    while True:
        try:
            return distutils.util.strtobool(input().lower())
        except ValueError:
            print('Please reply "y" or "n".')


def verifRoot():
    """
        Verify if euid 0, if not, exit(1)
    """
    if int(os.geteuid()) != 0:
        execo.log.logger.critical('You are not root. Use sudo or biroute.')
        sys.exit(1)


def verifyOrCreateDir(filename):
    """
        Verify if path exist (before a file will be created)
        create path if doesn't exist
    """
    execo.log.logger.debug(filename)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
        except OSError as e:
            execo.log.logger.critical('Cannot create directory: ' + e.strerror)
            sys.exit(1)


def executeProcess(commande):
    """
        execute commande, return nothing
    """
    with execo.process.Process(commande).run() as process:
        if process.exit_code != 0:
            execo.log.logger.critical('process:\n' + str(process))
            execo.log.logger.warning('process stdout:\n' + process.stdout)
            execo.log.logger.warning('process stderr:\n' + process.stderr)


def eraseFile(fichier):
    """
        erase the file given as argument
    """
    try:
        os.remove(fichier)
        execo.log.logger.info(str(fichier) + ' erased')
    except EnvironmentError as e:
        execo.log.logger.warning('Environment Error: ' + e.strerror +
                                 ' ' + e.errno +
                                 ' ' + e.filename)


def get_options():
    """
        read parser and return args (as args namespace)
    """
    parser = argparse.ArgumentParser(description='Erase crontab and configuration of automated snapshots')
    parser.add_argument('-d', '--debug', action='store_true', help='toggle debug (default: no)')
    parser.add_argument('-l', '--log', action='store_true', help='toggle log (default: no)')
    parser.add_argument('-n', '--name', nargs=1, type=str, help='zfs filesystem name', required=True)
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
