 $Id: README_v0.4.md 1055 2019-07-03 09:04:25Z gruiick $
 SPDX-License-Identifier: BSD-2-Clause

# PSMN ZFS toolbox

PSMN's python3 zfs toolbox is a rewrite of a unmaintenable set of 
bash scripts (gZFS).

## TL;DR

The idea is to have automated rolling snapshots (and replicas) in a time 
frame (default is one by day, rolling on seven days), using a set of 
python3 scripts.

It use execo python3 module (pip3 install execo) from INRIA.

This is Work In Progress:
    v0.1 tested (svn r837 - r859)
    v0.2 tested (svn r916 - r921)
    v0.3 tested (svn r935 - r936)
    v0.4 tested (svn r946 - r950)
    v0.5 ongoing dev

## Default directories and files (v0.4)

/root
├── conf
│   ├── replica-data-volume.yml (see zfs-conf-example.yml)
│   ├── snapshot-data-volume.yml (see zfs-conf-example.yml)
│   └── zfs-defaults.yml
└── tools
    ├── disable-replica.py
    ├── disable-snapshot.py
    ├── enable-replica.py
    ├── enable-snapshot.py
    ├── zfs-replica.py (need exec bit)
    └── zfs-snapshot.py (need exec bit)


## default configuration file (zfs-defaults.yml, v0.4)

```
%YAML 1.1
---
# crontab user
user: 'root'
# will be use for crontab creation date (empty)
date: ''
version: '0.3'
path:
  # main tools location
  snapshot: '/root/tools/zfs-snapshot.py'
  replica: '/root/tools/zfs-replica.py'
  # path to config dir (trailing / is mandatory)
  # home of zfs-defaults.yml
  conf: '/root/conf/'
  # path to crontab dir (trailing / is mandatory)
  cron: '/etc/cron.d/'
  # path to log dir (trailing / is mandatory)
  log: '/var/log/'
  # path for zfs binary (if not autodetected)
  zfs: ''
```

If you adapt to your needs, be sure to modify python scripts to use your
configuration.


## Snapshots

### enable-snapshot.py (v0.4)

 * interactive
 * create the yaml config file
 * create the crontab file
 * restart cron for new crontab to be handled

```
usage: enable-snapshot.py [-h] [-d] [-l] -n NAME [-r R] [-f | -v] 
       -H HOUR -m MINUTE [-DM DAYOFMONTH] [-M MONTH] [-DW DAYOFWEEK]

Configuration and crontab for automated snapshots

optional arguments:
  -h, --help            show this help message and exit
  -f                    zfs type: filesystem (default)
  -v                    zfs type: volume

conf:
  configuration file options

  -d, --debug           toggle debug (default: no)
  -l, --log             toggle logs (default: no)
  -n NAME, --name NAME  zfs filesystem name
  -r R                  retention, in day(s) (default: 7)

cron:
  crontab options

  -H HOUR, --hour HOUR  cron hour parameter
  -m MINUTE, --minute MINUTE
                        cron minute parameter
  -DM DAYOFMONTH, --dayofmonth DAYOFMONTH
                        cron day of month parameter
  -M MONTH, --month MONTH
                        cron month parameter
  -DW DAYOFWEEK, --dayofweek DAYOFWEEK
                        cron day of week parameter
```

example: `python3 enable-snapshot.py -n data/volume -H 1 -m 0`

Will create a configuration yaml file, to do a snapshot at 01:00 everyday, 
for zfs dataset 'data/volume', with a retention of 7 days, without log, 
debug and recapitulative mail. Will also create the corresponding crontab, 
and can activate it by restarting cron service.

example: `python3 enable-snapshot.py --log -r 15 --name data/volume -H 22 -m 15 -DW 0`

Will create a configuration yaml file, to do a snapshot at 22:15 every Sunday, 
for zfs dataset 'data/volume', with a retention of 15 days, with log, without 
debug and recapitulative mail. Will also create the corresponding crontab, 
and can activate it by restarting cron service.

If you need to run snapshot more than one by day, you should create a simple
crontab file, then modify it to your needs (man crontab), then restart 
cron service.

Be aware that zfs-snapshot.py erase snapshot on a daily basis, hence all 
snapshots created the same day, will be erased **snapshot day + retention**
days later.

It might be necessary for the firsts runs to activate the mail option. 
After some successfull tests, you can add '>/dev/null' manually in the 
crontab.

### zfs-snapshot.py (v0.4)

 * executed by cron (or interactive)
 * read the yaml config file
 * create a snapshot as configured
 * if necessary, erase some snapshot(s), as configured

```
usage: zfs-snapshot.py [-h] -c CONF [-d]

Create and delete snapshots

optional arguments:
  -h, --help            show this help message and exit
  -c CONF, --conf CONF  Mandatory config file
  -d, --debug           toggle debug (default: no)
```

example: `zfs-snapshot.py -c /root/conf/snapshot-data-volume.yml`

Will create/destroy snapshots for zfs dataset 'data/volume', according to
configuration.

Be aware that zfs-snapshot.py erase snapshot on a daily basis, hence all 
snapshots created the same day, will be erased **snapshot day + retention**
days later.

### disable-snapshot.py (v0.4)

 * interactive
 * erase the crontab file
 * restart cron
 * erase the yaml config file, if asked to

```
usage: disable-snapshot.py [-h] [-d] [-l] -n NAME

Erase crontab and configuration of automated snapshots

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           toggle debug (default: no)
  -l, --log             toggle log (default: no)
  -n NAME, --name NAME  zfs filesystem name
```

example: `disable-snapshot.py -n data/volume`

Will erase crontab, configuration file for zfs volume 'data/volume', and
restart cron service. Remaining snapshots will have to be erased manually.


## Replicas

### enable-replica.py (v0.4)

 * interactive
 * create the yaml config file
 * create the crontab file
 * restart cron for new crontab to be handled

```
usage: enable-replica.py [-h] [-d] [-l] -n NAME [-t TARGET] [-r RETENTION]
                         [--mail [MAIL]] [-s SERVER] [--simple | --complete]
                         [--latest | --oldest] -H HOUR -m MINUTE
                         [-DM DAYOFMONTH] [-M MONTH] [-DW DAYOFWEEK]

Configuration and crontab for automated replica

optional arguments:
  -h, --help            show this help message and exit
  --simple              replica type: simple, only target pool and last
                        dataset (default)
  --complete            replica type: complete, target pool plus complete
                        source dataset
  --latest              replica archive: only latest snapshot (default)
  --oldest              replica archive: complete set of snapshots

conf:
  configuration file options

  -d, --debug           toggle debug (default: no)
  -l, --log             toggle logs (default: no)
  -n NAME, --name NAME  zfs source volume name
  -t TARGET, --target TARGET
                        zfs target pool name (default: None, same as source)
  -r RETENTION, --retention RETENTION
                        retention, in day(s) (default: 7)
  --mail [MAIL]         let cron send an email each replica (default: no,
                        [default: root])
  -s SERVER, --server SERVER
                        target server hosting replication [default: localhost]

cron:
  crontab options:

  -H HOUR, --hour HOUR  cron hour parameter
  -m MINUTE, --minute MINUTE
                        cron minute parameter
  -DM DAYOFMONTH, --dayofmonth DAYOFMONTH
                        cron day of month parameter
  -M MONTH, --month MONTH
                        cron month parameter
  -DW DAYOFWEEK, --dayofweek DAYOFWEEK
                        cron day of week parameter
```

example: `python3 enable-replica.py -n data/volume -t backup -H 1 -m 0`

Will create a configuration yaml file, to do a replica at 01:00 everyday, 
of zfs dataset 'data/volume', to zfs dataset 'backup/volume' with a 
retention of 7 days, without log, debug and recapitulative mail, on
'localhost', simple and latest mode by default. 
Will also create the corresponding crontab, and can activate it by 
restarting cron service.


example: `python3 enable-replica.py --log -s server2 -r 15 --name data/volume -H 23 -m 15 -DW 0`

Will create a configuration yaml file, to do a replica at 23:15 every Sunday, 
for zfs dataset 'data/volume', with a retention of 15 days, with log, without 
debug and recapitulative mail, on host 'server2', simple and latest mode by 
default. Will also create the corresponding crontab, and can activate it 
by restarting cron service.

If you need to run replica more than one by day, you should create a simple
crontab file, then modify it to your needs (man crontab), then restart 
cron service.

Be aware that zfs-replica.py erase replica(s) on a daily basis, hence all 
replicas created the same day, will be erased **replica day + retention**
days later.

It might be necessary for the firsts runs to activate the mail option. 
After some successfull tests, you can add '>/dev/null' manually in the 
crontab.

### zfs-replica.py (v0.4)

 * executed by cron (or interactive)
 * read the yaml config file
 * propagate dataset and replica(s) as configured
 * if necessary, erase some replica(s), as configured

```
usage: zfs-replica.py [-h] -c CONF [-d]

Replicate and delete automated snapshots

optional arguments:
  -h, --help            show this help message and exit
  -c CONF, --conf CONF  Mandatory config file
  -d, --debug           toggle debug (default: no)
```

example: `zfs-replica.py -c /root/conf/replica-data-volume.yml`

Will copy/destroy replicas for zfs dataset 'data/volume', according to
configuration.

Be aware that zfs-replica.py erase replica on a daily basis, hence all 
replicas created the same day, will be erased **replica day + retention**
days later.

### disable-replica.py (v0.4)

 * interactive
 * erase the crontab file
 * restart cron
 * erase the yaml config file, if asked to

```
usage: disable-replica.py [-h] [-d] [-l] -n NAME

Erase crontab and configuration of automated replicas

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           toggle debug (default: no)
  -l, --log             toggle log (default: no)
  -n NAME, --name NAME  zfs filesystem name
```

example: `disable-replica.py -n data/volume`

Will erase crontab, configuration file for zfs dataset 'data/volume', and
restart cron service. Remaining replica, snapshots and dataset, will have
to be erased by hand on target hosting.

## Various / Bulk

  * logs

Logging use execo formatter, also in log files, which add colors with escape 
sequences. It may look weird with some pagers (less, without 'export LESS=-ir').

Logged interactive operations have a different color (orange) than 
automated operations (white).

  * debug

Debug might be extremely verbose. We advise to not debug AND log 
automaticaly for a long period of time.

